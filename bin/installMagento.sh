#!/bin/bash

docker-compose exec app bin/magento setup:install \
--base-url="http://magento.test/" \
--db-host="192.168.1.64" \
--db-name="magento" \
--db-user="root" \
--db-password="" \
--admin-firstname="admin" \
--admin-lastname="admin" \
--admin-email="user@example.com" \
--admin-user="admin" \
--admin-password="1234567_W" \
--language="en_GB" \
--currency="GBP" \
--timezone="America/Chicago" \
--use-rewrites="1" \
--backend-frontname="admin"